package com.example.wordgame;

import com.example.wordgame.ui.game_question.GameQuestionFragment;
import com.example.wordgame.ui.level_completed.LevelCompletedFragment;
import com.example.wordgame.ui.start_window.StartWindowFragment;

public class MainPresenter implements MainContract.Presenter {
    private MainContract.View mView;


    public MainPresenter(MainContract.View view){
        this.mView = view;
    }

    @Override
    public void onDetach() {
        mView = null;

    }

    @Override
    public void onGameLoaded() {
        mView.loadFragment(StartWindowFragment.newInstance());
    }

    @Override
    public void onStartGameButtonClicked() {
        mView.loadFragment(GameQuestionFragment.newInstance());
    }

    @Override
    public void onQuestionsFinish() {
        mView.loadFragment(LevelCompletedFragment.newInstance());
    }

}
