package com.example.wordgame;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;


public class MainActivity extends AppCompatActivity implements MainContract.View {
    final private String TAG = this.getClass().getSimpleName();
    private MainContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();

        presenter = new MainPresenter(this);
        presenter.onGameLoaded();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDetach();
    }

    public void loadFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.nav_host_fragment, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void startGameButtonClick() {
        Log.i(TAG, "public void startGameButtonClick()");
        presenter.onStartGameButtonClicked();
    }

    @Override
    public void onQuestionsFinish() {
        presenter.onQuestionsFinish();
    }

    @Override
    public void onGameFinish() {
        finish();
    }
}
