package com.example.wordgame;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

public class AnswerAdapter extends ArrayAdapter<Answer> {
    private Context context;
    private LayoutInflater inflater;
    private int layout;
    private List<Answer> answers;

    public AnswerAdapter(@NonNull Context context, int resource, @NonNull List<Answer> answers){
        super(context, resource, answers);
        this.context = context;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
        this.answers = answers;
    }

    public View getView(int position, View convertView, @NonNull ViewGroup parent){
        View view = inflater.inflate(this.layout, parent, false);
        ImageView imageView = view.findViewById(R.id.answerIcon);
        TextView textView = view.findViewById(R.id.answerText);

        Answer answer = answers.get(position);

        imageView.setImageResource(context.getResources().getIdentifier(answer.getImage(), "drawable", context.getPackageName()));
        textView.setText(answer.getTitle());


        return view;
    }
}
