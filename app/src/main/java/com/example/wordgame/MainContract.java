package com.example.wordgame;

import androidx.fragment.app.Fragment;

import com.example.wordgame.ui.game_question.GameQuestionFragment;
import com.example.wordgame.ui.start_window.StartWindowFragment;

public interface MainContract {
    interface View extends StartWindowFragment.OnStartGameClickListener, GameQuestionFragment.onQuestionsInteractHandler {
        void loadFragment(Fragment fragment);
    }

    interface Presenter {
        void onDetach();

        void onGameLoaded();

        void onStartGameButtonClicked();

        void onQuestionsFinish();
    }
}
