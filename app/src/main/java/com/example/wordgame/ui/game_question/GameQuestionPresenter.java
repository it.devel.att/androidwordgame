package com.example.wordgame.ui.game_question;

import com.example.wordgame.Answer;
import com.example.wordgame.Question;

import java.util.List;

public class GameQuestionPresenter implements GameQuestionContract.Presenter {
    final private String TAG = this.getClass().getSimpleName();

    private GameQuestionContract.View mView;
    private List<Question> questions = Question.generateQuestions();
    private Question currentQuestion;
    private int currentQuestionNumber = 0;

    GameQuestionPresenter(GameQuestionContract.View view) {
        this.mView = view;
        this.currentQuestion = questions.get(currentQuestionNumber);
    }


    @Override
    public void onDetach() {
        this.mView = null;
    }

    @Override
    public void onAnswerClick(int answerNumber) {
        mView.hideToastMessage();

        if (answerNumber == currentQuestion.getRightAnswerNumber()) {
            nextQuestionOrFinishGame();
        } else {
            mView.showToastMessage("Неправильный ответ");
        }
    }


    @Override
    public void onContinueClick() {
        nextQuestionOrFinishGame();
    }

    @Override
    public void onCloseButtonClick() {
        mView.finishGame();
    }

    @Override
    public void onViewCreated() {
        updateFragmentData();
    }

    @Override
    public List<Answer> getAnswers() {
        return currentQuestion.getAnswers();
    }

    @Override
    public String getCurrentQuestionTitle() {
        return currentQuestion.getTitle();
    }

    @Override
    public int getCurrentRoundNumber() {
        return currentQuestionNumber + 1;
    }

    @Override
    public int getTotalRoundNumber() {
        return questions.size();
    }

    private void updateFragmentData() {
        mView.updateRoundProgressBar();
        mView.updateRoundCounter();
        mView.updateCurrentQuestionTitle();
        mView.updateAnswersGallery();
    }

    private void nextQuestionOrFinishGame() {
        currentQuestionNumber += 1;

        if (currentQuestionNumber + 1 > questions.size()) {
            mView.showLevelCompleted();
        } else {
            currentQuestion = questions.get(currentQuestionNumber);
            updateFragmentData();
        }
    }
}
