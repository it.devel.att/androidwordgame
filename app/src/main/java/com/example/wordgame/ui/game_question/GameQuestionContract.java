package com.example.wordgame.ui.game_question;


import com.example.wordgame.Answer;

import java.util.List;

public interface GameQuestionContract {
    interface View {
        void showToastMessage(String message);

        void hideToastMessage();

        void updateRoundCounter();

        void updateRoundProgressBar();

        void updateCurrentQuestionTitle();

        void updateAnswersGallery();

        void showLevelCompleted();

        void finishGame();
    }

    interface Presenter {
        void onDetach();

        void onAnswerClick(int answerNumber);

        void onContinueClick();

        void onCloseButtonClick();

        void onViewCreated();

        List<Answer> getAnswers();

        String getCurrentQuestionTitle();

        int getCurrentRoundNumber();

        int getTotalRoundNumber();
    }
}
