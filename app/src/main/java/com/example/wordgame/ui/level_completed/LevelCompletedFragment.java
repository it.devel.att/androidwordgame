package com.example.wordgame.ui.level_completed;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.wordgame.R;

public class LevelCompletedFragment extends Fragment {
    final private String TAG = this.getClass().getSimpleName();

    public LevelCompletedFragment() {

    }

    public static LevelCompletedFragment newInstance() {
        return new LevelCompletedFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.level_completed_fragment, container, false);
    }
}
