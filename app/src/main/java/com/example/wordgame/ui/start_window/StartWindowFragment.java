package com.example.wordgame.ui.start_window;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.wordgame.R;

public class StartWindowFragment extends Fragment implements View.OnClickListener{
    private final String TAG = this.getClass().getSimpleName();

    OnStartGameClickListener listener;

    public StartWindowFragment() {

    }

    public static StartWindowFragment newInstance() {
        return new StartWindowFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (OnStartGameClickListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_window_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Button startGameButton = view.findViewById(R.id.start_game_button);
        startGameButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Log.i(TAG, "public void onClick(View v)");
        if (v.getId() == R.id.start_game_button)
            Log.i(TAG, "v.getId() == R.id.start_game_button");
            listener.startGameButtonClick();
    }

    public interface OnStartGameClickListener {
        void startGameButtonClick();
    }
}
