package com.example.wordgame.ui.game_question;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.wordgame.AnswerAdapter;
import com.example.wordgame.R;

public class GameQuestionFragment extends Fragment implements GameQuestionContract.View, View.OnClickListener {
    final private String TAG = this.getClass().getSimpleName();
    private GameQuestionContract.Presenter presenter;
    private View currentView;
    private Toast mToast;

    private ProgressBar mProgressBar;
    private TextView mRoundCounter;
    private TextView mCurrentQuestionTitle;
    private GridView mAnswersGallery;
    private onQuestionsInteractHandler questionsFinishHandler;

    public GameQuestionFragment() {
        this.presenter = new GameQuestionPresenter(this);
    }

    public static GameQuestionFragment newInstance() {
        return new GameQuestionFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        questionsFinishHandler = (onQuestionsInteractHandler) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.game_question_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        currentView = view;

        mProgressBar = view.findViewById(R.id.game_progress_bar);
        mRoundCounter = view.findViewById(R.id.roundCounter);
        mAnswersGallery = view.findViewById(R.id.answersGallery);
        mCurrentQuestionTitle = view.findViewById(R.id.questionTitle);

        Button continueButton = view.findViewById(R.id.continueButton);
        continueButton.setOnClickListener(this);

        ImageButton closeGameButton = view.findViewById(R.id.finishGameButton);
        closeGameButton.setOnClickListener(this);

        mAnswersGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                presenter.onAnswerClick(position);
            }
        });

        presenter.onViewCreated();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.onDetach();
        questionsFinishHandler = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        currentView = null;
    }


    @Override
    public void showToastMessage(String message) {
        mToast = Toast.makeText(currentView.getContext(), message, Toast.LENGTH_SHORT);
        mToast.show();
    }

    @Override
    public void hideToastMessage() {
        if (mToast != null)
            mToast.cancel();
    }


    @Override
    public void updateRoundCounter() {
        String roundCounter = String.format("%s/%s", presenter.getCurrentRoundNumber(), presenter.getTotalRoundNumber());
        mRoundCounter.setText(roundCounter);
    }

    @Override
    public void updateRoundProgressBar() {
        mProgressBar.setProgress(presenter.getCurrentRoundNumber());
        mProgressBar.setMax(presenter.getTotalRoundNumber());
    }

    @Override
    public void updateCurrentQuestionTitle() {
        mCurrentQuestionTitle.setText(presenter.getCurrentQuestionTitle());
    }

    @Override
    public void updateAnswersGallery() {
        AnswerAdapter answerAdapter = new AnswerAdapter(currentView.getContext(), R.layout.answer_block, presenter.getAnswers());
        mAnswersGallery.setAdapter(answerAdapter);
    }

    @Override
    public void showLevelCompleted() {
        questionsFinishHandler.onQuestionsFinish();
    }

    @Override
    public void finishGame() {
        questionsFinishHandler.onGameFinish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.continueButton:
                presenter.onContinueClick();
                break;
            case R.id.finishGameButton:
                presenter.onCloseButtonClick();
                break;
        }
    }

    public interface onQuestionsInteractHandler {
        void onQuestionsFinish();
        void onGameFinish();
    }
}
