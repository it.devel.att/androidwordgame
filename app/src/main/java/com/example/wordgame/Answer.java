package com.example.wordgame;

import java.util.ArrayList;
import java.util.List;

public class Answer {
    private String image;
    private String title;

    public Answer(String image, String title){
        this.image = image;
        this.title = title;
    }

    public static List<Answer> generateAnswers(){
        List<Answer> answers = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            answers.add(new Answer("watermelion", String.format("Answer %s", i + 1)));
        }
        return answers;
    }

    public String getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }
}
