package com.example.wordgame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Question {
    private String title;
    private List<Answer> answers;
    private int rightAnswerNumber;


    public Question(String title, List<Answer> answers, int rightAnswerNumber) {
        this.title = title;
        this.answers = answers;
        this.rightAnswerNumber = rightAnswerNumber;
    }

    public static List<Question> generateQuestions() {
        List<Question> questions = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            questions.add(new Question(String.format("Question %s", i + 1), Answer.generateAnswers(), random.nextInt( 4)));
        }
        return questions;
    }

    public String getTitle() {
        return title;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public int getRightAnswerNumber() {
        return rightAnswerNumber;
    }
}
